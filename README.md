# Monash Medicine assessment map

## Licence

Code is licensed under the [GNU Affero General Public License version 3](https://www.gnu.org/licenses/agpl-3.0.html) or later.

Data (data.yaml and derivatives) is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
