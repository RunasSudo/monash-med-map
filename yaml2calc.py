#!/usr/bin/python3

import odf.opendocument
import odf.style
import odf.table
import odf.text

import sys
import yaml2json

data, items = yaml2json.read_data()

unit = items[sys.argv[1]]

doc = odf.opendocument.OpenDocumentSpreadsheet()
for width in ['2.26cm', '10cm']:
	style = odf.style.Style(name='width'+width, family='table-column')
	style.addElement(odf.style.TableColumnProperties(columnwidth=width))
	doc.automaticstyles.addElement(style)

border_style = odf.style.Style(name='bordered', family='table-cell')
border_style.addElement(odf.style.TableCellProperties(border='0.75pt solid #000000'))
doc.automaticstyles.addElement(border_style)

bold_style = odf.style.Style(name='bolded', family='table-cell')
bold_style.addElement(odf.style.TextProperties(fontweight='bold'))
doc.automaticstyles.addElement(bold_style)

bold_right_style = odf.style.Style(name='boldedright', family='table-cell')
bold_right_style.addElement(odf.style.ParagraphProperties(textalign='end'))
bold_right_style.addElement(odf.style.TextProperties(fontweight='bold'))
doc.automaticstyles.addElement(bold_right_style)

sheet = odf.table.Table(name=sys.argv[1])
sheet.addElement(odf.table.TableColumn(stylename='width10cm'))
sheet.addElement(odf.table.TableColumn(numbercolumnsrepeated=5,stylename='width2.26cm'))

def build_row(*cells):
	row = odf.table.TableRow()
	for cell in cells:
		row.addElement(cell)
	return row

sheet.addElement(build_row(
	odf.table.TableCell(),
	odf.table.TableCell(stylename='bolded', stringvalue='Score'),
	odf.table.TableCell(stylename='bolded', stringvalue='Weight'),
	odf.table.TableCell(stylename='bolded', stringvalue='Borderline'),
	odf.table.TableCell(stylename='bolded', stringvalue='Scaled'),
	odf.table.TableCell(stylename='bolded', stringvalue='Aggregate')
))

rownr = 1

def do_item(item):
	global rownr
	if len(item['children']) > 0:
		for child in item['children']:
			do_item(child)
	else:
		rownr += 1
		sheet.addElement(build_row(
			odf.table.TableCell(stringvalue=item['name']),
			odf.table.TableCell(valuetype='percentage', stylename='bordered') if item['pct'] != 0 else odf.table.TableCell(),
			odf.table.TableCell(valuetype='percentage', value=item['pct']/unit['pct']),
			odf.table.TableCell(valuetype='percentage', value=item['borderline']) if 'borderline' in item else odf.table.TableCell(),
			odf.table.TableCell(valuetype='percentage', formula='==IF(D{0}; IF(B{0} > D{0}; 0.5 + (B{0} - D{0}) * 0.5 / (1 - D{0}); B{0} * 0.5 / D{0}); B{0})'.format(rownr)), # note semicolons, otherwise Err:508
			odf.table.TableCell(valuetype='percentage', formula='==E{0}*C{0}'.format(rownr))
		))

do_item(unit)

sheet.addElement(build_row(
	odf.table.TableCell(numbercolumnsspanned=5, stringvalue='Total', stylename='boldedright'),
	odf.table.CoveredTableCell(),
	odf.table.CoveredTableCell(),
	odf.table.CoveredTableCell(),
	odf.table.CoveredTableCell(),
	odf.table.TableCell(stylename='bolded', valuetype='percentage', formula='==SUM(F2:F{0})'.format(rownr))
))

doc.spreadsheet.addElement(sheet)

doc.save('int/' + sys.argv[1], True)
