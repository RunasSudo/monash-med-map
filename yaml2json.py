#!/usr/bin/python3

import hashlib
import json
import markdown
import yaml

def read_data():
	with open('data.yaml', 'r') as fin:
		data = yaml.load(fin)
	
	# Preprocess data
	items = {}
	def process_item1(item, parent):
		# Defaults
		if 'parent' not in item:
			item['parent'] = parent
		if 'type' not in item:
			item['type'] = 'assessment'
		if 'children' not in item:
			item['children'] = []
		if 'id' not in item:
			if 'code' in item:
				item['id'] = item['code']
			else:
				item['id'] = hashlib.sha256((item['parent']['id'] + item['name']).encode('utf-8')).hexdigest()
		items[item['id']] = item
		if 'unit' not in item:
			item['unit'] = item
			while item['unit']['type'] != 'unit' and item['unit']['parent'] is not None:
				item['unit'] = item['unit']['parent']
		if 'description' not in item:
			item['description'] = ''
		
		# Render description
		item['description'] = markdown.markdown(item['description'], extensions=['markdown.extensions.tables'], output_format='html5')
		# Nasty hack
		item['description'] = item['description'].replace('<table>', '<table class="ui celled table">')
		
		if 'hurdle' not in item:
			item['hurdle'] = False
		if item['parent'] is not None and 'weight' not in item:
			item['weight'] = None
		if 'fullname' not in item:
			if 'code' in item:
				item['fullname'] = item['code'] + ': ' + item['name']
			else:
				item['fullname'] = item['name']
		
		# Process children
		for child in item['children']:
			process_item1(child, item)
		
		# Auto-calculate weight
		if 'weight' in item and item['weight'] is None:
			item['weight'] = sum(child['weight'] for child in item['children'])
		
		# Percentage is in terms of previous component with weight
		if item['parent'] is not None and 'of' not in item:
			item['of'] = item['parent']
			# Recurse tree until we find one with a weight defined
			while item['of']['parent'] is not None and item['of']['weight'] is None:
				item['of'] = item['of']['parent']
	
	def process_item2(item):
		if 'of' in item and type(item['of']) is str:
			item['of'] = items[item['of']]
		item['pct'] = pctOf(item, data)
		
		if item['parent'] is None:
			item['cumPct'] = 0
		
		# Calculate cumulative percentages of children
		cumPct = item['cumPct']
		for child in item['children']:
			child['cumPct'] = cumPct
			process_item2(child) # Sneaky optimisation! ;)
			cumPct += child['pct']
	
	def process_item3(item):
		del item['parent']
		if 'of' in item and type(item['of']) is not str:
			item['of'] = item['of']['id']
		if 'unit' in item and type(item['unit']) is not str:
			item['unit'] = item['unit']['id']
		
		for child in item['children']:
			process_item3(child)
	
	def pctOf(item, base):
		if item is base:
			return 1
		if item['of'] is base:
			return item['weight']
		return item['weight'] * pctOf(item['of'], base)
	
	process_item1(data, None)
	process_item2(data)
	process_item3(data)
	
	return data, items

if __name__ == '__main__':
	data, _ = read_data()
	with open('data.json', 'w') as fout:
		print(json.dumps(data), file=fout)
